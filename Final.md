- **Module Code:** CS1SE20
- **Assignment report Title:**  
- **Student Number:** 28015918
- **Date (when the work completed):** 11/03/2021
- **Actual hrs spent for the assignment:** 10 hrs
- **Assignment evaluation (3 key points):**
    - Better understanding of agile methoddology.
    - Practing UML code.
    - Better understanding of feasibility study.


# Introduction 

Gaming and the internet have become ubiquitous technology. The reason why it has gained popularity is due to enjoyment and relaxation.  These factors can be exploited to create a game that is engaging and interesting at the same time. [1] shows improvement in student learning when different methods of teaching with the use of gaming. 
The sole reason for video games is entertainment, while on the other hand studying historical sources is the opposite for most if not everyone. Therefore, creating a game for educational purposes to create an enjoyable learning experience so that all students are engaged seems appropriate. Moreover, the Internet has become a major part in day to day of university students. It has become a norm to a point where most of the learning is done. With these two aspects combined the project will develop a web game that is accessible, flexible and enjoyable at the same time. 

Gaming makes tasks like repetition which is usually unexciting, entertaining with the use of audio, pleasing aesthetics and other engineered features. When entertaining tasks are repeated it helps with a better understanding of the topic. It is also likely that students will retain the knowledge for long periods which can be advantageous in future. 

The game is aimed for university students that are learning about historical events. It will contain historical sources that the students must read and understand to solve a puzzle which will be the main aspect of the game. Based on their solutions the game will record their score while will be used to benchmark and assess students learning. Tracking score requires a login which will be achieved through students university credentials. This will also be used for identifying each individuals score. 

# Feasibility

## Legal and Social Feasibility

Sometimes certain design or trademark can already exist and creating such aspects can cause legal issues which usually lead to a hefty fine. Therefore before implementing the designs extensive research should be done and legal guidelines should be followed strictly

As mentioned before the content of the game will be based on historical events thus it is of utmost importance for the sources to be accurate. For this reason, the information will be extracted through reputable books and scholars. Currently, there are exceptions to UK copyright law, one of which is for teaching which allows minor acts of copying to lawfully be made. Moreover, displaying webpages or quotes without having to “seek additional permissions” also falls under this exception [2]. In addition to that, the sources will be accompanied by a sufficient acknowledgement to follow the copyright guidelines.

Along with the video, audio is another element that makes the overall experience of the game immersive and enjoyable as a result it is a factor that needs consideration as well. Although the game requires reading text to understand for some individuals the audio might be distracting. Nevertheless, the game will contain audio and since there is no music producer, it will require licensing an audio package. Because the game is not used for commercial purposes, the fee for the licensing will be paid as a set price instead of a percentage of earning. 

Another constraint is that content of the game can be offensive to certain groups of people considering the events that have occurred in the past, while on the other hand it is also important to learn such topic to understand how the past has shaped the present and how it will affect the future. All things considered, the game content should be appropriate and educational at the same time.

As a new system will be implemented it will require the lecturer to learn the basic understanding and controls of the game which then they will pass to the students during a dedicated session. Furthermore, gameplay and other information will be provided in the game on a dedicated web page which will be accessible through the game menu.

## Economic Feasibility 

Programmers are one of the most critical people in game development. They are the reason why the game work as required. They bring every element of the game together to make the game functional. Therefore, it only makes sense to invest a good proportion of the budget towards an experienced programmer. Moreover, modern web-based games are coded, using various programming language. It should also be noted that increasing the number of programmers working on a project will consequently increase confusion in maintaining and working on the code. Furthermore, the game is not as complex as the modern triple AAA titles so it can be considered as a small game where one experienced programmer and one inexperienced should suffice. The experienced programmer will focus on the style and scripting of the game whereas the other programmer will focus on the layout.

A good storyline is also very crucial especially for this type of project where there is a timeline. For instance, if the random events are presented is a disordered manner it can be very confusing. Presenting historic events according to the timeline will provide a context that will make sense. Therefore, a good narrative will help students to better understand the sources which for essential to solving the puzzles to earn points. It is also likely to help students to retain the knowledge. Since the sources have already happened there is no need for an original narrative and the sources only need to be in a time-ordered fashion, hiring a single narrative designer is sufficient.

The benefits of software testing are paramount. Two of the main aspect of project development time and money can be saved if appropriate testing is conducted. These free resources can be used in other parts of the project. However, it is necessary to begin testing as soon as possible because sooner the bug is found sooner it can be fixed consequently leading to saving more of the important resources. Having two software testers for a project of this size seems relevant. Each tester will perform different testing types from the early stages of the project to maximise efficiency for mentioned reasons. 

## Estamate total cost for the company  
Salary based per year.
1 graphics specialist, £ 80,000 
1 narrative designer, £ 50 000 
2 software tester, £ 50,000 x2
1  experienced programmer (CSS, JAVASCRIPT), £ 100,000
1 Inexperienced prgrammer (HTML), £ 56,000
3 assistant worker 78,000
Office space (250x12) x 9 =  £ 27,000
Total etstamted cost = £ 491000

The total cost for creating the game will be 15% of the total estimated £ 73,650 cost.

The game will be hosted through the university server because it increases performance since most of the learning will be while in university thus decreasing the latency. Furthermore, all the student data will already be saved in the university server therefore it does not require connection to a different server if hosted in the same server.  More importantly, it is free. 
Maintaining the game will be covered for six months after the deployment for no extra cost due to one year contract with the employee however after that period it will become the university’s responsibility to maintain the game however all the documentation and code will be provided to the client.

## Technical feasibility 

HTML will be used to build the layout of the web site and the CSS for the aesthetics and finally JavaScript for scripting the game. The gherkin language will be used for project documentation and tests. For hardware most modern computers are capable of running a simple game so it should not be an issue, while for the web browser all devices have them pre-installed. Internet should be available to most since it has become a mandatory technology for learning.

## Development methodology 

Customer satisfaction is a must in the modern day of software development. The client wants the project to be finished within 6 months with at least three fully functional scenarios, since the deadline for the project is short considering the amount of work the needs to be completed, an agile methodology is an appropriate choice. This approach is designed to produce the software faster and accommodate change by taking advantage of short development cycles known as "sprints" that focuses on feedback from stakeholders for continuous improvement.

The framework for the methodology will be scrum because it makes for effective collaborations among teams. Each "sprints" mentioned before will consist of a two to three week time period. During the sprint, the most important features will be built first (fully functional scenarios).

The agile values also come in handy when developing the software.
Individual and interaction over processes and tools.
The project has many different pieces. Each piece of the project requires individuals with experience in that field to compete for their task. Then pieces need to be put together, thus the interactions between these individuals must be a precedence.

Documentation is an important process of software development but comprehensive documentation can be time consuming and because the project deadline is short, producing a working software over comprehensive documentation should be the highest priority. Not to say the documentation is ignored completely but instead, it will be simplified so that the developers can start developing as soon as possible. 

With agile methodology, if the client wants to update certain requirements after the project has already started it can be managed. During sprints, features can be adjusted based on the client's feedback so when the final product is delivered it is as required by the client.

## Modelling, Design, and Implementation 

# Use cases.
<img title="Use cases (Figure 1)" 
src="Image/usercase.png">

# Sequence diagram of playing game.
<img title="Sequence diagram of playing game (Figure 2)"
src="Image/Sequence.png">

# Logical system architecture.
<img title="Logical system architecture (Figure 3)"
src="Image/logical.png">

## Reflection 

Learning about project management and game development is a vital skill that can be applied to most real-world scenario. Especially in the era of the internet where everything relies on software. Engineering and managing these projects are an arduous and time-consuming task, that requires unique skills to complete. There has been a substantial growth increase in the gaming industry in the last decade and all game software and services are expected to rise further. Which will create opportunities for individuals that possess the knowledge. Also working as a team is a skill that is a fundamental part of any group-based project. But due to current circumstances, some limitations forced the team to use alternative methods of communication, mainly digital. As a result of this it was difficult to convey ideas however at the same it allowed the team to get customary with these alternatives considering most real-world scenario requires teams to communicate in such a manner. Overall, the project was a great learning experience on how to tackle a real-world problem. If a similar task was to be completed in the future, frequent communication with the team would be a top priority.

# References 
[Click here](https://csgitlab.reading.ac.uk/wm015918/cs1se20-proposal/-/blob/master/Reference.md%20)
